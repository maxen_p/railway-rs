package com.epam.railway;

import com.epam.railway.entity.Human;
import com.epam.railway.entity.Ticket;
import com.epam.railway.entity.TicketState;
import com.epam.railway.service.TicketService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Path("/tickets")
public class TicketResource {

    private TicketService ticketService = new TicketService();

    @GET
    @Path("/init")
    public void init() {
        for (int i = 1; i < 3; i++) {
            Ticket ticket = new Ticket();
            ticket.setDepartureCity("departCity#" + i);
            ticket.setArrivalCity("arrivalCity#" + i);
            ticket.setDepartureTime(LocalDateTime.now());
            ticket.setArrivalTime(LocalDateTime.now().plus(i, ChronoUnit.DAYS));
            ticket.setPrice(10 + i);
            ticket.setState(TicketState.BOOKED);
            ticket.setPassenger(new Human("sname#" + i, "fname#" + i,
                    "mname" + i, LocalDate.now().plus(i, ChronoUnit.YEARS)));
            ticketService.bookTicket(ticket);
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Ticket> listTickets() {
        return ticketService.getAll();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Ticket retrieveTicket(@PathParam("id") long id) {
        return ticketService.retrieveTicket(id);
    }

    @GET
    @Path("/{id}/passenger")
    @Produces(MediaType.APPLICATION_JSON)
    public Human retrieveHuman(@PathParam("id") long id) {
        return ticketService.retrieveTicket(id).getPassenger();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public long createTicket(Ticket ticket) {
        System.out.println(ticket);
        return ticketService.bookTicket(ticket);
    }

    @PUT
    @Path("/{id}")
    public void payForTicket(@PathParam("id") long id) {
        ticketService.payForTicket(id);
    }

    @DELETE
    @Path("/{id}")
    public void returnTicket(@PathParam("id") long id) {
        ticketService.removeTicket(id);
    }
}
