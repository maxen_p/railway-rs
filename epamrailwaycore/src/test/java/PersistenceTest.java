import com.epam.railway.entity.Human;
import com.epam.railway.entity.Ticket;
import com.epam.railway.entity.TicketState;
import com.epam.railway.persistence.PersistenceManager;
import org.junit.Test;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public class PersistenceTest {

    @Test
    public void jpaTest() {
        EntityManager entityManager = PersistenceManager.getInstance().getEntityManager();

        Human human = new Human("surname1", "firstname1", "middlename1", LocalDate.now());
        Ticket ticket = new Ticket();
        ticket.setArrivalCity("arrivalCite1");
        ticket.setArrivalTime(LocalDateTime.MAX);
        ticket.setDepartureCity("deptCite2");
        ticket.setDepartureTime(LocalDateTime.MIN);
        ticket.setPassenger(human);
        ticket.setPrice(34);
        ticket.setState(TicketState.BOOKED);

        entityManager.getTransaction().begin();
        entityManager.persist(ticket);
        entityManager.getTransaction().commit();
        entityManager.close();

        entityManager = PersistenceManager.getInstance().getEntityManager();
        entityManager.getTransaction().begin();
        List<Ticket> result = entityManager.createQuery("from Ticket", Ticket.class).getResultList();
        for (Ticket human1 : result) {
            System.out.println(human1);
        }
        entityManager.getTransaction().commit();
        entityManager.close();
        PersistenceManager.getInstance().close();
    }
}
