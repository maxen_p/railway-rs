package com.epam.railway.persistence;

import com.epam.railway.entity.Ticket;
import com.epam.railway.entity.TicketState;

import javax.persistence.EntityManager;
import java.util.List;

public class TicketDAO {

    private PersistenceManager persistenceManager;
    private EntityManager entityManager;

    private TicketDAO() {
        persistenceManager = PersistenceManager.getInstance();
        entityManager = persistenceManager.getEntityManager();
    }

    public long create(Ticket ticket) {
        entityManager.getTransaction().begin();
        entityManager.persist(ticket);
        long id = ticket.getId();
        entityManager.getTransaction().commit();
        return id;
    }

    public Ticket read(long id) {
        return entityManager.find(Ticket.class, id);
    }

    public void update(Ticket ticket) {
        entityManager.getTransaction().begin();
        Ticket ticket1 = entityManager.find(Ticket.class, ticket.getId());
        ticket1.setState(TicketState.PAID);
        entityManager.getTransaction().commit();
    }

    public void delete(long id) {
        entityManager.getTransaction().begin();
        Ticket ticket = entityManager.find(Ticket.class, id);
        entityManager.remove(ticket);
        entityManager.getTransaction().commit();
    }

    public List<Ticket> readAll() {
        return entityManager.createQuery("FROM Ticket", Ticket.class).getResultList();
    }

    private static class SingletonHolder {
        private static final TicketDAO instance = new TicketDAO();
    }

    public static TicketDAO getInstance() {
        return SingletonHolder.instance;
    }
}
