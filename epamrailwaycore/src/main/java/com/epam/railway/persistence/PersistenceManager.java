package com.epam.railway.persistence;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class PersistenceManager {

    private EntityManagerFactory entityManagerFactory;

    private PersistenceManager() {
        entityManagerFactory = Persistence.createEntityManagerFactory("com.epam.railway.core");
    }

    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    public void close() {
        entityManagerFactory.close();
    }

    private static class SingletonHolder {
        private static final PersistenceManager instance = new PersistenceManager();
    }

    public static PersistenceManager getInstance() {
        return SingletonHolder.instance;
    }
}
