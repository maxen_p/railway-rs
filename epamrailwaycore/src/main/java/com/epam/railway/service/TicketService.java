package com.epam.railway.service;

import com.epam.railway.entity.Ticket;
import com.epam.railway.entity.TicketState;
import com.epam.railway.persistence.TicketDAO;

import java.util.List;

public class TicketService {

    private TicketDAO dao = TicketDAO.getInstance();

    public long bookTicket(Ticket ticket) {
        ticket.setState(TicketState.BOOKED);
        return dao.create(ticket);
    }

    public Ticket retrieveTicket(long id) {
        return dao.read(id);
    }

    public void payForTicket(long id) {
        Ticket ticket = dao.read(id);
        dao.update(ticket);
    }

    public void removeTicket(long id) {
        dao.delete(id);
    }

    public List<Ticket> getAll() {
        return dao.readAll();
    }
}
