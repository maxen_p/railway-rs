package com.epam.railway.entity;

import com.epam.railway.persistence.xmladapter.LocalDateAdapter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "humans")
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Human {
    private long id;
    private String surname;
    private String firstname;
    private String middlename;
    private LocalDate birthday;

    public Human() {
    }

    public Human(String surname, String firstname, String middlename, LocalDate birthday) {
        this.surname = surname;
        this.firstname = firstname;
        this.middlename = middlename;
        this.birthday = birthday;
    }

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    @Column(name = "human_id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public LocalDate getBirthday() {
        return birthday;
    }

    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return id == human.id &&
                Objects.equals(surname, human.surname) &&
                Objects.equals(firstname, human.firstname) &&
                Objects.equals(middlename, human.middlename) &&
                Objects.equals(birthday, human.birthday);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, surname, firstname, middlename, birthday);
    }

    @Override
    public String toString() {
        return "Human{" +
                "id=" + id +
                ", surname='" + surname + '\'' +
                ", firstname='" + firstname + '\'' +
                ", middlename='" + middlename + '\'' +
                ", birthday=" + birthday +
                '}';
    }
}
