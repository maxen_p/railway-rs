package com.epam.railway.entity;

import com.epam.railway.persistence.xmladapter.LocalDateTimeAdapter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "tickets")
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Ticket {
    private long id;
    private String departureCity;
    private String arrivalCity;
    private LocalDateTime departureTime;
    private LocalDateTime arrivalTime;
    private int price;
    private TicketState state;
    private Human passenger;

    public Ticket() {
    }

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    @Column(name = "ticket_id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(String departureCity) {
        this.departureCity = departureCity;
    }

    public String getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(String arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    public LocalDateTime getDepartureTime() {
        return departureTime;
    }

    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    public void setDepartureTime(LocalDateTime departureTime) {
        this.departureTime = departureTime;
    }

    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    public LocalDateTime getArrivalTime() {
        return arrivalTime;
    }

    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    public void setArrivalTime(LocalDateTime arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public TicketState getState() {
        return state;
    }

    public void setState(TicketState state) {
        this.state = state;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "human_id")
    public Human getPassenger() {
        return passenger;
    }

    public void setPassenger(Human passenger) {
        this.passenger = passenger;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ticket ticket = (Ticket) o;
        return id == ticket.id &&
                price == ticket.price &&
                Objects.equals(departureCity, ticket.departureCity) &&
                Objects.equals(arrivalCity, ticket.arrivalCity) &&
                Objects.equals(departureTime, ticket.departureTime) &&
                Objects.equals(arrivalTime, ticket.arrivalTime) &&
                state == ticket.state &&
                Objects.equals(passenger, ticket.passenger);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, departureCity, arrivalCity, departureTime, arrivalTime, price, state, passenger);
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", departureCity='" + departureCity + '\'' +
                ", arrivalCity='" + arrivalCity + '\'' +
                ", departureTime=" + departureTime +
                ", arrivalTime=" + arrivalTime +
                ", price=" + price +
                ", state=" + state +
                ", passenger=" + passenger +
                '}';
    }
}
