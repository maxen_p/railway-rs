package com.epam.railway.entity;

public enum TicketState {
    BOOKED, PAID, EXPIRED;
}
