package com.epam.railway;

import com.epam.railway.entity.Ticket;
import org.glassfish.jersey.client.ClientConfig;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.List;

public class RSClient {

    ClientConfig config = new ClientConfig();
    Client client = ClientBuilder.newClient(config);
    WebTarget target = client.target(getBaseURI());

    public Ticket getTicket(long id) {
        return target.path(String.valueOf(id)).request().get(Ticket.class);
    }

    public List<Ticket> getAll() {
        return (List<Ticket>) target.request().get(List.class);
    }

    public long bookTicket(Ticket ticket) {
        return target.request().post(Entity.entity(ticket, MediaType.APPLICATION_JSON),
                Long.class);
    }

    public void payForTicket(Ticket ticket) {
        target.path(String.valueOf(ticket.getId())).request().put(Entity.entity(ticket, MediaType.APPLICATION_JSON),
                Long.class);
    }

    public void deleteTicket(long id) {
        target.path(String.valueOf(id)).request().delete();
    }

    private static URI getBaseURI() {
        return UriBuilder.fromUri("http://localhost:9080/tickets").build();
    }
}
